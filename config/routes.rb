Rails.application.routes.draw do
  post 'users' => 'users#create'
  post 'auth/login' => 'authentications#create'

  namespace :api do
    namespace :v1 do
      resources :notes, except: :show
    end
  end
end
