class NoteBlueprint < Blueprinter::Base
  identifier :id

  fields :title, :content
end
