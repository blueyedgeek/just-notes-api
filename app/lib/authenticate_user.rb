class AuthenticateUser
  attr_reader :token

  def initialize(email:, password:)
    @email = email
    @password = password
    @token = nil
  end

  def call
    if valid_credentials?
      @token = JsonWebToken.encode(payload: { email: user.email })
    end
  end

  private

  attr_reader :email, :password

  def user
    User.find_by(email: email)
  end

  def valid_credentials?
    user&.authenticate(password)
  end
end
