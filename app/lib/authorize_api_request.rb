class AuthorizeApiRequest
  def initialize(access_token)
    @access_token = access_token
  end

  def call
    user
  end

  private

  attr_reader :access_token

  def user
    decoded_access_token = decode_access_token(access_token)

    @user ||= User.find_by(email: decoded_access_token['email'])
  end

  def decode_access_token(token)
    JsonWebToken.decode(token: token).first
  end
end
