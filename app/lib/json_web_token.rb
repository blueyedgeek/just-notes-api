class JsonWebToken
  def self.encode(payload:, secret: secret_key, algorithm: 'HS256',
                  exp: 24.hours.from_now.to_i)
    JWT.encode(payload, secret, algorithm, exp: exp)
  end

  def self.decode(token:, secret: secret_key, algorithm: 'HS256')
    JWT.decode(token, secret, true, algorithm: algorithm)
  end

  private_class_method

  def self.secret_key
    ENV['AUTH_SECRET_KEY']
  end
end
