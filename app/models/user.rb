class User < ApplicationRecord
  has_secure_password

  has_many :notes

  validates :email,
            presence: true,
            format: { with: URI::MailTo::EMAIL_REGEXP },
            uniqueness: { case_sensitive: false }
  validates :password,
            length: { minimum: 8 }
  validates :password_confirmation, presence: true

end
