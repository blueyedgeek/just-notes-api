class Note < ApplicationRecord
  default_scope { order(updated_at: :desc) }

  belongs_to :user

  validate :ensure_title_or_content_present

  def ensure_title_or_content_present
    if title.blank? && content.blank?
      errors.add(:title_and_content, 'cannot both be blank')
    end
  end
end
