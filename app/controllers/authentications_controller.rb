class AuthenticationsController < ApplicationController
  skip_before_action :authorize_user

  def create
    email = authentication_params[:email]
    password = authentication_params[:password]
    user = AuthenticateUser.new(email: email, password: password)

    user.call

    if user.token
      json_response(
        response: { access_token: user.token },
        status: :ok
      )
    else
      json_response(
        response: { message: 'Invalid credentials' },
        status: :unprocessable_entity
      )
    end
  end

  private

  def authentication_params
    params.permit(:email, :password)
  end
end
