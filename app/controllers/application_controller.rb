class ApplicationController < ActionController::API
  before_action :authorize_user

  private

  def json_response(response:, status:)
    render json: response, status: status
  end

  def authorize_user
    if request.headers['Authorization'].present?
      current_user
    else
      json_response(response: { message: 'No access token present' },
                    status: :unauthorized)
    end
  end

  def current_user
    access_token = request.headers['Authorization'].split.last

    @current_user ||= AuthorizeApiRequest.new(access_token).call
  rescue StandardError
    json_response(response: { message: 'Access token is invalid' },
                  status: :unauthorized)
  end
end
