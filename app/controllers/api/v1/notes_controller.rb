class Api::V1::NotesController < ApplicationController
  def index
    notes = current_user.notes.map { |note| NoteBlueprint.render_as_hash(note) }

    json_response(response: notes, status: :ok)
  end

  def create
    note = current_user.notes.new(note_params)

    if note.save
      json_response(
        response: NoteBlueprint.render(note),
        status: :created
      )
    else
      json_response(
        response: {
          message: 'Failed to create new note',
          errors: note.errors
        },
        status: :unprocessable_entity
      )
    end
  end

  def update
    note = current_user.notes.find(params[:id])

    if note.update(note_params)
      json_response(
        response: NoteBlueprint.render(note),
        status: :ok
      )
    else
      json_response(
        response: {
          message: 'Failed to update note',
          errors: note.errors
        },
        status: :unprocessable_entity
      )
    end
  end

  def destroy
    note = current_user.notes.find(params[:id])

    note.destroy

    json_response(response: { message: 'Note deleted' }, status: :no_content)
  end

  private

  def note_params
    params.permit(:title, :content)
  end
end
