class UsersController < ApplicationController
  skip_before_action :authorize_user

  def create
    user = User.new(user_params)

    if user.save
      json_response(
        response: { message: 'Account created sucessfully, you can now login' },
        status: :created
      )
    else
      json_response(
        response: {
          message: 'Failed to register new user',
          errors: user.errors.full_messages
        },
        status: :unprocessable_entity
      )
    end
  end

  private

  def user_params
    params.permit(:email, :password, :password_confirmation)
  end
end
