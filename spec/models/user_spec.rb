require 'rails_helper'

RSpec.describe User do
  it { is_expected.to have_many(:notes) }

  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
  it { is_expected.to allow_value('test@example.com').for(:email) }
  it { is_expected.to_not allow_value('test').for(:email) }

  it { is_expected.to validate_presence_of(:password) }
  it { is_expected.to validate_length_of(:password).is_at_least(8) }

  it { is_expected.to validate_presence_of(:password_confirmation) }
end
