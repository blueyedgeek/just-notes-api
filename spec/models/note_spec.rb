require 'rails_helper'

RSpec.describe Note, type: :model do
  it { is_expected.to belong_to(:user) }

  describe 'Attributes of a note' do
    context 'when the title and content are not present' do
      it 'is expected to be an invalid record' do
        note = build(:note, title: nil, content: nil)

        expect(note).to be_invalid
      end
    end

    context 'when only a title is present' do
      it 'is expected to be a valid record' do
        note = build(:note, title: 'A title', content: nil)

        expect(note).to be_valid
      end
    end

    context 'when only content is present' do
      it 'is expected to be a valid record' do
        note = build(:note, title: nil, content: 'These are my resolutions')

        expect(note).to be_valid
      end
    end
  end
end
