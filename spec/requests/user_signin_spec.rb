require 'rails_helper'

RSpec.describe 'User Signs in to Application' do
  describe 'Authenticating a user' do
    let(:email) { 'test@example.com' }
    let(:password) { 's3cur3-p@ss' }

    before { create_new_user(email: email, password: password) }

    context 'when the specified parameters are valid' do
      it 'creates an authentication token for the user' do
        post auth_login_path, params: { email: email, password: password }

        expect(response).to have_http_status(:ok)
        expect(response_body).to have_key('access_token')
      end
    end

    context 'when any of the specified parameters are invalid' do
      it 'prevents access to the application' do
        post auth_login_path, params: { email: email, password: 'fake-pass' }

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response_body).to include('message' => 'Invalid credentials')
      end
    end
  end
end
