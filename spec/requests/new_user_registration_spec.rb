require 'rails_helper'

RSpec.describe 'New User Registration' do
  describe 'Creating a new user account' do
    let(:email) { 'test@example.com' }
    let(:password) { 's3cur3-p@ss' }
    let(:password_confirmation) { password }

    before do
      post users_path,
           params: { email: email,
                     password: password,
                     password_confirmation: password_confirmation }
    end

    context 'when the specified parameters are valid' do
      it 'successfully creates a new user account' do
        expect(response).to have_http_status(:created)
      end
    end

    context 'when the specified parameters are invalid' do
      context 'when the email is invalid' do
        let(:email) { '' }

        it 'does not create a new user account' do
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context 'when the password is invalid' do
        let(:password) { 'short' }

        it 'does not create a new user account' do
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end

      context 'when the password and password_confirmation don\'t match' do
        let(:password_confirmation) { 'divergent' }

        it 'does not create a new user account' do
          expect(response).to have_http_status(:unprocessable_entity)
        end
      end
    end
  end
end