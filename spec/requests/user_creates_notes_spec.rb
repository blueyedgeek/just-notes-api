require 'rails_helper'

RSpec.describe 'User Creates Notes' do
  describe 'Creating a note' do
    let(:email) { 'test@example.com' }
    let(:password) { 's3cur3-p@ss' }
    let(:title) { 'Note title' }
    let(:content) { 'Note content' }

    before do
      user = create_new_user(email: email, password: password)
      @token = login(email: user.email, password: password)
    end

    context 'when the specified parameters are valid' do
      it 'successfully creates a new note record for the user' do
        post api_v1_notes_path,
             params: { title: title, content: content },
             headers: { 'Authorization' => "Bearer #{@token}" }

        expect(response).to have_http_status(:created)
        expect(response_body).to include(
          'title' => 'Note title', 'content' => 'Note content'
        )
      end
    end

    context 'when the specified parameters are invalid' do
      let(:title) { nil }
      let(:content) { nil }

      it 'does not create a new note record for the user' do
        post api_v1_notes_path,
             params: { title: title, content: content },
             headers: { 'Authorization' => "Bearer #{@token}" }

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response_body).to include(
          'errors' => { 'title_and_content' => ['cannot both be blank'] }
        )
      end
    end
  end
end
