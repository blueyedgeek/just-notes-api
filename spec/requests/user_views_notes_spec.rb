require 'rails_helper'

RSpec.describe 'User Views Notes' do
  describe 'Viewing a user created note' do
    let(:email) { 'test@example.com' }
    let(:password) { 's3cur3-p@ss' }

    it 'displays the notes belonging to a user' do
      user = create_new_user(email: email, password: password)
      token = login(email: user.email, password: password)
      create_note_for(user: user,
                      note_title: 'A new day',
                      note_content: 'It is a new day')

      get api_v1_notes_path,
          headers: { 'Authorization': "Bearer #{token}" }

      expect(response).to have_http_status(:ok)
      expect(response_body.first).to include('title' => 'A new day')
      expect(response_body.first).to include('content' => 'It is a new day')
    end

    def create_note_for(user:, note_title:, note_content:)
      create(:note, user: user, title: note_title, content: note_content)
    end
  end
end
