require 'rails_helper'

RSpec.describe 'Unauthorized Requests by a User' do
  describe 'Accessing a protected route' do
    context 'when no access token is provided' do
      it 'prevents user access' do
        get api_v1_notes_path

        expect(response).to have_http_status(:unauthorized)
        expect(response_body).to include('message' => 'No access token present')
      end
    end

    context 'when an invalid or expired access token is provided' do
      it 'prevents user access' do
        get api_v1_notes_path, headers: { 'Authorization' => 'Bearer 12345678' }

        expect(response).to have_http_status(:unauthorized)
        expect(response_body).to include('message' => 'Access token is invalid')
      end
    end
  end
end
