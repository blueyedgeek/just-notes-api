require 'rails_helper'

RSpec.describe 'User Deletes Notes' do
  describe 'deleting a note' do
    let(:email) { 'test@example.com' }
    let(:password) { 's3cur3-p@ss' }
    let(:title) { 'Note title' }
    let(:content) { 'Note content' }

    before do
      @user = create_new_user(email: email, password: password)
      @token = login(email: @user.email, password: password)
      @note = create(:note, user: @user, title: title, content: content)
    end

    it 'removes the note from the database' do
      delete api_v1_note_path(@note),
             headers: { Authorization: "Bearer #{@token}" }

      expect(response).to have_http_status(:no_content)
      expect(@user.notes).to be_empty
    end
  end
end
