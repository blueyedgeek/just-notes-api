require 'rails_helper'

RSpec.describe 'User Updates Notes' do
  describe 'updating a note' do
    let(:email) { 'test@example.com' }
    let(:password) { 's3cur3-p@ss' }
    let(:title) { 'Note title' }
    let(:content) { 'Note content' }

    before do
      user = create_new_user(email: email, password: password)
      @token = login(email: user.email, password: password)
      @note = create(:note, user: user, title: title, content: content)
    end

    it 'updates the title' do
      patch api_v1_note_path(@note),
            params: { title: 'Updated title' },
            headers: { Authorization: "Bearer #{@token}" }

      expect(response).to have_http_status(:ok)
      expect(response_body).to include('title' => 'Updated title')
    end

    it 'updates the content' do
      patch api_v1_note_path(@note),
            params: { content: 'Updated content' },
            headers: { Authorization: "Bearer #{@token}" }

      expect(response).to have_http_status(:ok)
      expect(response_body).to include('content' => 'Updated content')
    end

    context 'when the attributes are invalid' do
      it 'does not update the note' do
        patch api_v1_note_path(@note),
              params: { title: nil, content: nil },
              headers: { 'Authorization' => "Bearer #{@token}" }

        expect(response).to have_http_status(:unprocessable_entity)
        expect(response_body).to include(
          'errors' => { 'title_and_content' => ['cannot both be blank'] }
        )
      end
    end
  end
end
