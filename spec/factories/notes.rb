FactoryBot.define do
  factory :note do
    title { 'Note Title' }
    content { 'Energy is the product of mass and the speed of light squared' }
    user { create(:user) }
  end
end
