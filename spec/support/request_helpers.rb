module RequestHelper
  def response_body
    JSON.parse(response.body)
  end

  def create_new_user(email:, password:)
    create(:user,
           email: email,
           password: password,
           password_confirmation: password)
  end

  def login(email:, password:)
    post auth_login_path, params: { email: email, password: password }

    response_body['access_token']
  end
end
